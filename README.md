# spring-boot-angularjs-starter #

Simple starter for Spring Boot and AngularJS apps

## To run ##

Run 

`./gradlew bootRun `

from the project root. App will start on localhost:8080, serving the angularJS application from Spring Boot's static resources.

## To package ##

Run 

`./gradlew clean build`

from the project root. Outputs jar to `build/libs/`

## Configuration ##

Project uses the Moowork Node gradle plugin to bind the npm install tasks to gradle's compileJava task.

`npm install` will output the built files to `src/main/resources/static`, which gradle's build then packages into the Spring Boot jar to be served as static content.

The output directory for `npm install` is configured via src/main/angular-app/.angular-cli.json under apps.outDir

## Notes ##

- Add the Spring Boot Dev Tools for backend hot reloading
- You can run the backend and frontend separately with

`./gradlew booRun`

from the project root to serve the backend (and currently packaged AngularJS app) on localhost:8080.

Go to 

`src/angular-app` 

and run

`ng serve`

to run the frontend independently.

## Misc Dependencies ##
In order to use the commands specified in the auto-generated readme at `src/main/angular-app` you will need the `angular-cli` installed.

Install via npm with 

`npm install -g @angular/cli`

