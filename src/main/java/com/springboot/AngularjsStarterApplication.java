package com.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularjsStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularjsStarterApplication.class, args);
	}
}
